public class Main {
    public static void main(String[] args) {

      User newUser = new User("Tee Jae", "Calinao", 25, "Antipolo City");

      Course java101 = new Course();
      java101.setName("Intro to Java");
      java101.setDescription("Learn Java");
      java101.setSeats(30);
      java101.setFee(500.00);
      java101.setStartDate("April 25, 2022");
      java101.setEndDate("April 29, 2022");
      java101.setInstructor(newUser);

        System.out.println("User's first name:");
        System.out.println(newUser.getFirstName());
        System.out.println("User's last name:");
        System.out.println(newUser.getLastName());
        System.out.println("User's age:");
        System.out.println(newUser.getAge());
        System.out.println("User's address:");
        System.out.println(newUser.getAddress());

      System.out.println("Course's name:");
      System.out.println(java101.getName());
      System.out.println("Course's description:");
      System.out.println(java101.getDescription());
      System.out.println("Course's fee");
      System.out.println(java101.getFee());
      System.out.println("Course's instructor's first name:");
      System.out.println(java101.getInstructorName());

    }
}